var path = require("path");
var babelConfig = require('./babel.config');

module.exports = {
  context: __dirname,
  entry: [
    './src/index.js',
  ],
  output: {
    path: path.join(__dirname, 'build', 'static'),
    publicPath: "/static/",
    filename: "app.js",
    library: 'MyFlux',
  },
  resolve: {
    extensions: ['', '.js'],
  },
  resolveLoader: {
    'fallback': path.join(path.dirname(__dirname), 'node_modules'),
  },
  module: {
    loaders: [
      {include: /\.json$/, loaders: ["json-loader"]},
      {
        test: /\.js$/,
        loader: 'babel', //runtime is required for es6-generators
        exclude: /node_modules/,
        query: babelConfig,
      },
    ],
  },
};
