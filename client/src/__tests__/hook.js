var _ = require('lodash');
var babelConfig = require('../../babel.config');
var config = _.clone(babelConfig);
config.plugins = ['babel-plugin-rewire', ...babelConfig.plugins];
module.exports = require('babel-register')(config);
