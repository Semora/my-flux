var webpack = require('webpack');
var baseConfig = require('../webpack-base.config');

var config = Object.create(baseConfig);
config.entry = [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    ...config.entry,
];

config.plugins = [
    new webpack.HotModuleReplacementPlugin(),
];

module.exports = config;
