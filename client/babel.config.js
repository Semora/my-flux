var path = require('path');
var src = path.resolve(__dirname, 'src');
module.exports = {
    presets: ['es2015', 'react'],
    plugins: [
        'transform-object-rest-spread',
        'transform-runtime',
        ['babel-plugin-module-alias', [
            {src, 'expose': 'src'},    // this address is relative to $CWD
        ]],
    ],
};
